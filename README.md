# 5GPPP TMV Basic Testing Bundle

This project provides a basic testing bundle to help developers and test engineers quickly get started building and performing tests using OpenTAP as sequencing engine.
The project provides 2 containers; one with OpenTAP, and one with tools installed.
- The OpenTAP container comes with a number of open-source plugins installed (also from the 5GENESIS project [github.com/5genesis](https://github.com/5genesis)), which can be used to control and interact with tools or entities, but also with a TapPlans editor (TUI) to create and modify test plans.
- The Tools container comes with ssh, iperf3 and ping-utils installed.

Finally the project provides a docker-compose file and an example OpenTAP TapPlans. These form an example of how a simple infrastructure can be deployed with 3 containers (1 OpenTAP and 2 tools) and how OpenTAP can be used to run test actions between tools.

This project presents a minimum basic setup of OpenTAP and tools, where the system under test (SUT) is the network betweeb the 2 tools containers deployed. The concept is easily extended by including more tools (and OpenTAP plugins to control them) and connecting the tools to more or bigger SUTs. The concept architecture of OpenTAP, plugins, tools and SUT is depicted below:

![Concept architecture of OpenTAP, plugins, tools and SUT](images/concept_architecture.png)

## Basic Testing Bundle Usage

Set values for IMAGE_OPENTAP and IMAGE_TOOLS in a file `.env` next to `docker-compose.yml` file, e.g.: 

`IMAGE_OPENTAP=registry.gitlab.com/opentap/plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/opentap:0.2.1-beta.3`

`IMAGE_TOOLS=registry.gitlab.com/opentap/plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/tools:0.2.1-beta.3`

For available versions of containers see [gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/container_registry](https://gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/container_registry)

Use docker-compose and the docker-compose.yml ([gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/-/blob/master/test/docker-compose.yml](https://gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/-/blob/master/test/docker-compose.yml)) file to launch an example stack:

`$ docker-compose up -d`


Once the stack is launched the example OpenTAP TapPlans (included in the OpenTAP container) can be executed:

`$ docker exec -i opentap tap run -v --settings /opt/tap/Settings/hellotest-bench -e "Test Duration DL"=12 -e "Test Duration UL"=14 /opt/tap/hellotest-iperf-example.TapPlan`

(hint: to print OpenTAP help use `$ docker exec -i opentap tap -h`)


To create or edit OpenTAP TapPlans use the included Textual User Interface (TUI) ([packages.opentap.io/index.html#/?name=TUI&version=0.1.0-beta.96%2B7b56eceb&os=Windows,Linux&architecture=AnyCPU](https://packages.opentap.io/index.html#/?name=TUI&version=0.1.0-beta.96%2B7b56eceb&os=Windows,Linux&architecture=AnyCPU)):

`$ docker exec -it opentap tap tui`

(hint: use `F9` to browse menu items)


## OpenTAP plugins

The opentap container comes preinstalled with the following list of plugins:
- SSH*
- Iperf*
- TUI*
- Yardstick*
- OpenStackHeat*
- 5Genesis Plugins (from [github.com/5genesis/TAP-plugins](https://github.com/5genesis/TAP-plugins))

(* = available at [packages.opentap.io/index.html](https://packages.opentap.io/index.html))

### OpenTAP plugins development

To get started on OpenTAP plugin development there are a number of relevant resources that should be explored:

- OpenTAP can be downloaded and installed from [OpenTAP.io](https://www.opentap.io/). It also contains links to the community pages, documentation, news, and it hosts a package repository where opentap packages can be downloaded from.
- On Gitlab.com there are a number of plugins released as open-source which could be used for inspiration: [gitlab.com/OpenTAP/Plugins](https://gitlab.com/OpenTAP/Plugins)
- On youtube there are a number of video guides and tutorials available hosted by OpenTAP channel ([youtube.com/channel/UCPVG-eaRTg9pZ063LnjvnFg](https://www.youtube.com/channel/UCPVG-eaRTg9pZ063LnjvnFg)) and also from other sources such as 5G-VINNI ([youtube.com/channel/UCJvEIH7_8ocLyVOmwLDldwA](https://www.youtube.com/channel/UCJvEIH7_8ocLyVOmwLDldwA))

## Modifying the example

It is easy to expand the example provided here, for instance by

<details><summary>Adding more plugins to the OpenTAP container to control other tools</summary>
Install existing plugins in the OpenTAP container either by modifying the OpenTAP container dockerfile or by installing after deployment using:

`$ docker exec -it opentap tap package install [package]`
Additionally you can develop your own plugins by using the Developer's System OpenTAP bundle and following the guide provided by OpenTAP.
</details>

<details><summary>Changing the OpenTAP TapPlans to perform other actions toward the tools</summary>
The example OpenTAP TapPlans ([gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/-/blob/master/opentapplans/hellotest-iperf-example.TapPlan](https://gitlab.com/OpenTAP/Plugins/5g-ppp-test-measurement-and-kpis-validation-wg/5gppp-tmv-basic-testing-bundle/-/blob/master/opentapplans/hellotest-iperf-example.TapPlan)) is included in the OpenTAP container. This can be modified using the Textual UI (TUI) plugin which is also installed. Try out modifying test step settings or setting parameters as external parameters, which can be given as input when executing the test TapPlans (see Basic Testing Bundle Usage for example).
</details>

<details><summary>Adding different tools to the tools container</summary>
The tools container comes with some basic tools preinstalled but more can easily be added either by modifying the toolscontainer dockerfile, or by installing after deployment using linux package manager:

`apt install [package]`.
</details>

<details><summary>Deploying the tools containers in different settings to accomodate the specific scenario</summary>
The tools containers are in the docker-compose example deployed together with the OpenTAP container. This allows for easy access via shared namespace. Alternatively the tools containers can be deployed on other hosts or in other clusters - this however means that routing between OpenTAP container and tools containers must be ensured by the engineer.
</details>


<details><summary>Integrating with Yardstick for NFVI testing</summary>
The OpenTAP container comes with Yardstick plugin installed. To connect with the Yardstick tool a Yardstick instrument must be configured via the Yardstick plugin. The Yardstick tool can be deployed as a container ([hub.docker.com/r/opnfv/yardstick/](https://hub.docker.com/r/opnfv/yardstick/)), e.g. as part of the docker-compose file. Likewise, an influxdb ([hub.docker.com/_/influxdb](https://hub.docker.com/_/influxdb)) container can be deployed for result storage, and a Grafana ([hub.docker.com/r/grafana/grafana](https://hub.docker.com/r/grafana/grafana)) container for result visualization.
</details>




