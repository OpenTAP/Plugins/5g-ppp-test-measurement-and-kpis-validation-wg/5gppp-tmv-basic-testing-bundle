# OpenTAP container

The opentapcontainer is based on the public slim OpenTAP image from Docker Hub: [https://hub.docker.com/r/opentapio/opentap/tags?page=1&ordering=last_updated](url)

In the container image a number of open-source OpenTAP plugins are installed - please inspect the Dockerfile for specific versions and details.
Also an example OpenTAP plan is added and a settings profile containing settings for the Iperf and SSH instruments.
