# Tools container

The toolscontainer is based on the public Ubuntu Bionic image from Docker Hub: [https://hub.docker.com/_/ubuntu](url)

In the container image is also installed openssh-server, iperf3 and iputils-ping.
Additionally a user is added with username/password: test/test, which is used in the example OpenTAP plan included in this project.
Finally ssh service and iperf3 server are set to start at runtime.
